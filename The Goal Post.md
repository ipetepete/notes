---
tags:
  - Goals
  - Ideas
---
# Wants
- [ ] trip to Carlsbad caverns 
- [x] trip to SD

# Side gig

- [x] research side gig ideas 15-20min 2 times a week for two weeks - make a list of potential gig ideas
- [x] discuss with wife after 2 weeks
- [x] make a plan for side gig after 2 weeks


# Get Truck running

### Clean the garage #garage
We need a place to work

- [x] empty out tool chest
- [x] repurpose or sell the chest
- [ ] find a home for chest contents
- [ ] get rid of excess materials
- [x] hang foam on garage door
- [x] put away paint etc
- [ ] park the cooler out of the way

### Transmission
- [ ] order transmission guts
- [ ] drain transmission 
- [ ] drop transmission 
- [ ] rebuild transmission 
