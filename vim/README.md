## VIM

Simple setup with basic autocomplete, fuzzy-search and code tag jumping

      set nocompatible " Don't emulate vi
      set wildmenu     " Shows tabbed auto complete when finding files
      set path+=**     " Allows for recursive finding files
      filetype plugin indent on
      " Mo'better mode switching
      inoremap fd <ESC> 
      let mapleader = ","

This simple setup gets a lot for free, no plugins.

## Some usefull commands
- `:read /path/to/file` inserts the contents of the file into the current buffer
- `:set var?` show the contents of a vim variable
- `:find:*filename` finds files matching the pattern and opens it (if only one matches)
- `C-]` jump to tag definition\*
- `C-T` jump back
- `:tag [pattern]` will jump to the tag matching the pattern
- `:ls` list buffers
_Completion list_
-`C-x C-n` symbols in current file
-`C-x C-f` files/paths
-`C-x C-]` tags only
-`C-n` next in list

__*__Run `ctags -R .` in the current working root directory 



