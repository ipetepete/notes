---
id: Work ToDo
aliases: []
tags: []
---


## To Do
#### 2024-05-08

- [x] Fix `marsnat`
- [x] Work through current state of the extension
- [x] Get it to download without crashing if a file is unavailable
- [ ] Update popup while downloads are happening 
- [ ] Track total bytes downloaded
- [ ] Set the Nginx path for downloads on production

---

## Vue Update

- [ ] Refactor `mixins` to use a normal data store + functional utils
- [ ] Refactor separate components into SFCs
- [ ] Generate new project in codebase to use new tooling
- [ ] Update all the dependencies
- [ ] Update storage/session 
- [ ] Separate apps for search, results, download?
- [ ] Switch from JS to TS?

- Keep an eye on https://v3-migration.vuejs.org

### Notes for the day

Goal: Get at least two of these done today
Level up: Get all the things done

