---
id: 1724870154-KOBY
aliases:
  - CFT Estimate
tags: []
---

Biggest hurdles:
- Calculations
- Updating factors
- Uploading CSV
- Summary reports
- Multi-step forms

Smaller tasks:
- Export data
- Serializers
- RO "freeze" GHGI
- Tests


Based on prior knowledge of the French's work, most of the functional components are easy to outline.

Time to complete Biggest hurdles for MVP:
(1 person full-time)
__3 Weeks__ +/- 5-7 days for potential roadblocks etc.

Time for smaller tasks:
__1 week__  +/- 2-3 days

Utilizing Django and all it's built in functionality and several plugins with some light front end work using HTMX instead of a full blown front end framework will cut down significantly on development work.

Final polishing:
- graphics
- styling
- UX tweaks
- backend scripts/management

2-3 weeks (can be spread out over the course of development and into production cycles)

Total: 6 weeks +/- 2 weeks for production, deployable system.
