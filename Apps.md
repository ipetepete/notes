---
tags:
  - Code
  - Ideas
---
### TheaterCats
better name?

MVP
- login
- theater list
- show list
- filter by city/theater/datetime
- search for cast/director
- show detail view
	- photos
	- cast
	- runtime
	- reviews
	- director
	- writer
	- factoid
- theater detail view
	- photos
	- reviews
	- tips
	- parking
	- restrooms
	- kid friendly
	- seats/seat type
	- leg room
- map view for theaters
- show itinerary
- suggest shows

