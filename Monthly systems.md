## Create systems, not goals


### General
- declutter 
	- [x] kitchen
	- [x] work area
	- [x] bedroom
	- [ ] INPROGRESS garage
- fix backyard 
	- [ ] turf
	- [ ] edging
	- [ ] planter
	- [ ] rocks
	- [ ] irigation
- [x] get back into the Google training
- [x] start building better habits
- [x] develop a deeper relationship with the family
- [x] be more present
- [x] laugh more
- [x] stress less
- [x] set some achievable goals
- [x] read more
- [x] skate more

