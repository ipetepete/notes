#Learn 

# Resources/access

Resources four levels, resources, projects, folders, organization node

Policies at every level, inherited downward

Projects - enable, use services, apis etc

Projects multiple owners/users

Folders can contain projects, other folders or both
group resources per department & access policies

Org policy admin, project creator (roles)

Identity access management IAM
- Google account
- group
- cloud identity domain

IAM can have roles
 - basic
	 - most broad
 - predefined
	 - specific actions
 - custom
	 - like predefined, but can have custom permissions

Service account can be assigned roles and access to specific resources

Google Admin Console
- admins can disable/remove accounts & groups

Admin resources
- console
- shell
- cloud sdk
- apis
- cloud app

# VMs and networks

VPC combine scalability and convenience of public cloud with data isolation

Google VPC networks are global and can have subnets

Compute engine = IaaS
Full OS
Billed by the second min 1min

Premtable VMS - less features, 24h run max
Spot VMS - no run limit, same pricing for both

Max number of cpus per vm is tied to machine family, quota and zone.

Routing tables + firewall built in
VPC Peering can exchange traffic between projects

SSL, TCP proxy only works on some ports

Internal load balancer supports Proxy, Passthrough and Application load balancers

DNS can be managed via console, api, cli

Edge caching = Cloud CDN

Cloud VPN uses router 
Peering option, uses Point of Pressence exposes local network interconnection
Dedicated connection - Allows for direct connection 99.8%SLA
Partner Interconnect - if data cente ris in a physical location that can't reach dedicated colo
Cross Cloud interconnect - Establishes network between Google cloud and another cloud provider



