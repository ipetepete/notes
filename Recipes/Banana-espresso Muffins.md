
- 1c chickpea flour
- 3/4c all purpose GF flour 
- 1/4c protein powder
- 2 ripe! Bananas
- 1/2c yogurt/kefir 
- splash of milk or water ~2tbsp
- 3/4c ground up walnuts & choc. covered espresso beans
- 2 eggs
- 1 tsp vanilla
- 1/2c maple syrup
- 1/2c melted butter
- 1/2 tsp baking soda
- 1/2 tsp baking powder

Bake at 350 20-22 min
