---
tags:
  - goals
  - journal
---
# Prompts
## love
1. Who do you trust most? Why?
2. What are your strengths in relationships (kindness, empathy, etc.)?
3. How do you draw strength from loved ones?
4. What do you value most in relationships (trust, respect, sense of humor, etc.)?
5. What three important things have you learned from previous relationships?
6. What five traits do you value most in potential partners?
7. How do you show compassion to others? How can you extend that same compassion to yourself?
8. What are three things working well in your current relationship? What are three things that could be better?
9. What boundaries could you set in your relationships to safeguard your own well-being?
10. What do you most want your children (or future children) to learn from you?
11. How can you better support and appreciate your loved ones?
12. What does love mean to you? How do you recognize it in a relationship?
13. List three things you’d like to tell a friend, family member, or partner.
## career
1. How do you use your personal strengths and abilities at work?
2. How do your co-workers and supervisors recognize your strengths?
3. How does work fulfill you? Does it leave you wanting more?
4. What part of your workday do you most enjoy?
5. What about your work feels real, necessary, or important to you?
6. Do you see yourself in the same job in 10 years?
7. What are your career ambitions?
8. What three things can help you begin working to accomplish those goals?
9. What can you do to improve your work performance?
10. What does your work teach you? Does it offer continued opportunities for learning and growth?
11. Does your work drain or overwhelm you? Why? Is this something you can change?

## reflections 
1. What values do you consider most important in life (honesty, justice, altruism, loyalty, etc.)? How do your actions align with those values?
2. What three changes can you make to live according to your personal values?
3. Describe yourself using the first 10 words that come to mind. Then, list 10 words that you’d _like_ to use to describe yourself. List a few ways to transform those descriptions into reality.
4. What do you appreciate most about your personality? What aspects do you find harder to accept?
5. Explore an opinion or two that you held in the past but have since questioned or changed. What led you to change that opinion?
6. List three personal beliefs that you’re willing to reconsider or further explore.
7. Finish this sentence: “My life would be incomplete without …”
8. Describe one or two significant life events that helped shape you into who you are today.
9. When do you trust yourself most? When do you find it harder to have faith in your instincts?
10. What three things would you most like others (loved ones, potential friends and partners, professional acquaintances, etc.) to know about you?

## emotions
1. What difficult thoughts or emotions come up most frequently for you?
2. Which emotions do you find hardest to accept (guilt, anger, disappointment, etc.)? How do you handle these emotions?
3. Describe a choice you regret. What did you learn from it?
4. What parts of daily life cause stress, frustration, or sadness? What can you do to change those experiences?
5. What are three things that can instantly disrupt a good mood and bring you down? What strategies do you use to counter these effects?
6. What are three self-defeating thoughts that show up in your self-talk? How can you reframe them to encourage yourself instead?
7. What go-to coping strategies help you get through moments of emotional or physical pain?
8. Who do you trust with your most painful and upsetting feelings? How can you connect with them when feeling low?
9. What do you fear most? Have your fears changed throughout life?








# Plan
### Ideas
- gratitude
- reflections
- goals
- quotes
- language learning
	- vocab
	- grammar rule examples etc


Try the VOMIT system: 
https://youtu.be/U8RQsJ0Q3Mo?si=2KB19xVw4yo7Fafl

- Vent
	- start by unloading anything that is bothering
- obligations
	- dump all obligations/wants
	- order using a guiding question ex: which of these things, if done, make Al the others easier
	- bare minimum list / killing it stretch
- mental state
	- reframing - "how is this the best thing that has ever happened to me."
	- possibility - because statements
	- inversion - look at the opposite of a solution
	- perspective - view yourself from the outside
	- discipline - 
	- gratitude
- ideate
	- 5 minute come up with a list of 30 ideas to solve a problem
	- leave a question to simmer over night
- trajectory
	- goal
	- use evidence to graph if moving toward or away from goal
	- hidden metrics, make them visible 
		- what excited me
		- what drained me
		- what did I learn

### Notebooks:

- Silvine bullet - ?
- Yamamoto Ro-Biki Notebook - jetpens
- Baronfig

### Pens
- tomsstudio.com - lumps mini