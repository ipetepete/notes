---
Tags:
  - Goals
  - Reading
---


- [ ] Building a second brain
- [ ] Pragmatic Programmer
- [ ] PARA method
- [x] Atomic habits
- [ ] mindset - new psychology of success
- [ ] The Body Keeps the Score
- [ ] SAGA
- [ ] first lie wins 
- [ ] how to solve your own murder
- [x] the storm we made
- [ ] James
- [ ] The Bear
- [x] Stillness is the key
- [x] Found 
- [x] Holly
- [ ] blood meridian 
- [ ] hard rain falling
- [ ] Sun eater
- [ ] tress of the emerald sea
- [ ] one dark window
- [ ] someone you can build a nest in
- [ ] light bringer
- [ ] shutter