

Through play, I attract unlimited resources for all I want and need to do. I surrender to uncertainty and invite challenges that allow for unlimited potential. I am worthy of and have an abundance of joy, love and success.